// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

// storage: stored in Blockchain
// memory: exists within scope of function ()
// calldata: non-persistent and non-modifiable; only available to "external" functions (read-only)

// assignments:
// storage<>memory<>calldata (any conversion) => create an independent copy
// storage to a local storage => copy by reference
// memory<>memory => copy by reference

contract SimpleStorage {
    struct Person {
        string name;
        uint256 age;
    }

    // These are state variables,
    // which are stored in "storage" always
    // The default visibility of state variables is "internal"
    // Explicitly stating "internal" won't harm
    address internal masterAddress;
    address[] addresses;

    // "public " is visible for external contracts
    mapping(address => Person) public people;
    uint256 public peopleCount; // by default it initilizes to null (which 0 for numbers)
    address public adminAddress;

    // "constant" vs "immutable"
    // constant: must be fixed at compile time
    // immutable: must be assigned at construction time (only used for value type)
    string constant CONTRACT_NAME = "SimpleStorage";
    address immutable creatorAddress;

    constructor() {
        masterAddress = msg.sender;
        creatorAddress = msg.sender;
        adminAddress = msg.sender;
    }

    // @name is a string, which is an array of bytes in the nutshell,
    // so it is a "reference type", unlike premitive data types are called "value types"
    // Thefore, you should use "memory" as data storage as it will be discarded after executing the function
    function signup(string memory name, uint256 age) public {
        addresses.push(msg.sender);
        people[msg.sender] = Person({name: name, age: age});
        peopleCount += 1;
    }

    // Storage costs comparison:
    // @efficientAddressFetcher takes @addresses as storage and passes it to @useStorage as a ref
    // while @inefficientAddressFetcher give @addresses storage to @useMemory, which converts it to memory as a value
    function useStorage(address[] storage _addresses, uint256 index)
        internal
        view
        returns (address)
    {
        return _addresses[index];
    }

    function efficientAddressFetcher(uint256 index)
        external
        view
        returns (address)
    {
        return useStorage(addresses, index);
    }

    function useMemory(address[] memory _addresses, uint256 index)
        internal
        pure
        returns (address)
    {
        return _addresses[index];
    }

    function inefficientAddressFetcher(uint256 index)
        external
        view
        returns (address)
    {
        return useMemory(addresses, index);
    }
}
