// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./SimpleStorage.sol";
import "./node_modules/hardhat/console.sol";

contract StorageFactory is SimpleStorage {
    mapping(string => SimpleStorage) public storages;

    function createStorage(string memory storageName) public {
        // Storage name should not be taken!
        require(
            address(storages[storageName]) == address(0x0),
            string(
                abi.encodePacked(
                    'Storage of name "',
                    storageName,
                    '" already exists!'
                )
            )
        );

        SimpleStorage _storage = new SimpleStorage();
        storages[storageName] = _storage;
    }

    function adminAddressOf(string memory storageName)
        public
        view
        returns (address)
    {
        SimpleStorage _storage = storages[storageName];

        require(
            address(_storage) != address(0x0),
            string(
                abi.encodePacked('Storage "', storageName, '" does not exist!')
            )
        );
        return _storage.adminAddress();
    }
}
